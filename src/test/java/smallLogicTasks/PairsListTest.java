package smallLogicTasks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class PairsListTest {
    private List<Integer> list1 = List.of(1, 2, 3);
    private List<Integer> list2 = List.of(4, 5);
    private List<List<Integer>> expected = List.of(
            List.of(1, 4),
            List.of(1, 5),
            List.of(2, 4),
            List.of(2, 5),
            List.of(3, 4),
            List.of(3, 5)
    );

    @Test
    public void testPairsForEach() {
        List<List<Integer>> result = PairsList.pairsWithForEach(list1, list2);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testPairsStream() {
        List<List<Integer>> result = PairsList.pairsWithStreamApi(list1, list2);

        Assertions.assertEquals(expected, result);
    }

}