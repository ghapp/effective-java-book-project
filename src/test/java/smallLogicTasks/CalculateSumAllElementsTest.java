package smallLogicTasks;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculateSumAllElementsTest {

    @Test
    void calculateCount() {
        List<String> letters = "Hellow World !".chars()
                .mapToObj(c -> String.valueOf((char) c))
                .collect(Collectors.toList());

        assertEquals(14, CalculateSumAllElements.calculateCountElements(letters));
    }

}