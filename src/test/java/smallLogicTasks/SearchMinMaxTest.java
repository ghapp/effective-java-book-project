package smallLogicTasks;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SearchMinMaxTest {

    /**
     * Test of findMinMax method, of class SmallLogicTasks.SearchMinMax.
     */
    @Test
    public void testFindMinMax() {
        System.out.println("findMinMax");
        List<Integer> list = List.of(300, 200, 3, 4, 5);

        assertEquals(300, SearchMinMax.max(list));
        assertEquals(3, SearchMinMax.min(list));
    }

}