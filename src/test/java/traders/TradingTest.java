package traders;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import traders.model.Trader;
import traders.model.Transaction;

import java.util.Arrays;
import java.util.List;


class TradingTest {
    Trading trading;
    @BeforeEach
    public void init(){
        trading = new Trading();
    }

    @Test
    void transactions2011sortBySum() {
        List<Transaction> exp = Arrays.asList(
                new Transaction(new Trader("Brian","Cambridge"), 2011, 300),
                new Transaction(new Trader("Raoul", "Cambridge"), 2011, 400)
        );

        Assertions.assertIterableEquals(exp, trading.transactions2011sortBySum());
    }

    @Test
    void getCities() {
        Assertions.assertIterableEquals(Arrays.asList("Cambridge","Milan"), trading.getCities());
    }

    @Test
    void getCambridgeTradersSortedByName() {
        Assertions.assertIterableEquals(Arrays.asList(
                new Trader("Alan","Cambridge"),
                new Trader("Brian","Cambridge"),
                new Trader("Raoul", "Cambridge")),
                trading.getCambridgeTradersSortedByName());
    }

    @Test
    void traderNames() {
        Assertions.assertEquals("AlanBrianMarioRaoul", trading.traderNames());
    }

    @Test
    void milanTraderHave() {
        Assertions.assertTrue(trading.milanTraderHave());
    }

    @Test
    void allSumOfTransactionsFromCambridge() {
        Assertions.assertIterableEquals(List.of(300,1000,400,950), trading.allSumOfTransactionsFromCambridge());
    }

    @Test
    void maxSum() {
        Assertions.assertEquals(1000, trading.maxSum());
    }

    @Test
    void minSum() {
        Assertions.assertEquals(300, trading.minSum());
    }
}