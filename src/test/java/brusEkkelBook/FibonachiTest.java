package brusEkkelBook;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Collections;
import java.util.List;

class FibonachiTest {
    @Test
    public void fiboForEachTest(){
        Assertions.assertEquals(Collections.EMPTY_LIST, Fibonachi.generateFibonachiList(0));
        Assertions.assertEquals(List.of(0), Fibonachi.generateFibonachiList(1));
        Assertions.assertEquals(List.of(0, 1), Fibonachi.generateFibonachiList(2));
        Assertions.assertEquals(List.of(0, 1, 1, 2, 3, 5), Fibonachi.generateFibonachiList(6));
    }

    @Test
    public void fiboSteamTest(){
        Assertions.assertEquals(Collections.EMPTY_LIST, Fibonachi.generateFibonachiListWithStreamApi(0));
        Assertions.assertEquals(List.of(0), Fibonachi.generateFibonachiListWithStreamApi(1)); // 1 -> 0
        Assertions.assertEquals(List.of(0, 1), Fibonachi.generateFibonachiListWithStreamApi(2)); // 2 -> 0, 1
        Assertions.assertEquals(List.of(0, 1, 1, 2, 3, 5), Fibonachi.generateFibonachiListWithStreamApi(6)); // ...
    }

}