package traders;

import traders.model.Trader;
import traders.model.Transaction;

import java.util.*;
import java.util.stream.Collectors;

public class Trading {

    Trader raoul = new Trader("Raoul", "Cambridge");
    Trader mario = new Trader("Mario","Milan");
    Trader alan = new Trader("Alan","Cambridge");
    Trader brian = new Trader("Brian","Cambridge");

    List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300),
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950));


    // 1) найти транзакции за 2011 год отсортированные по сумме
    public List<Transaction> transactions2011sortBySum() {
        return transactions.stream()
                .filter(transaction -> transaction.getYear() == 2011)
                .sorted(Comparator.comparingInt(Transaction::getValue))
                .collect(Collectors.toList());
    }

    // 2) найти уникальные города, в которых работают трейдеры
    public List<String> getCities(){
        return transactions.stream().map(t->t.getTrader().getCity()).distinct()
                .collect(Collectors.toList());
    }

    // 3) все трейдеры из Cambridge, отсортированные по именам
    public List<Trader> getCambridgeTradersSortedByName(){
        return transactions.stream().map(Transaction::getTrader)
                .filter(trader -> trader.getCity().equals("Cambridge"))
                .distinct()
                .sorted(Comparator.comparing(Trader::getName))
                .collect(Collectors.toList());
    }

    // 4) Вернуть строку со всеми именами трейдеров отсортированными в алфавитном порядке
    public String traderNames (){
        return transactions.stream().map(t -> t.getTrader().getName())
                .distinct().sorted(String::compareTo)
                .collect(Collectors.joining());
    }

    // 5) Выяснить существует ли хоть один трейдер из милана
    public boolean milanTraderHave(){
        return transactions.stream().anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
    }

    // 6) Вывести суммы всех транзакций трейдеров из Кембриджа
    public List<Integer> allSumOfTransactionsFromCambridge(){
        return transactions.stream().filter(
                transaction -> transaction.getTrader().getCity().equals("Cambridge"))
                .map(Transaction::getValue)
                .collect(Collectors.toList());
    }

    // 7) Найти максимальную сумму среди всех транзакций
    public Integer maxSum(){
        return transactions.stream().mapToInt(Transaction::getValue).max().orElse(-1);
    }


    // 8) Найти транзакцию с минимальной суммой
    public Integer minSum(){
        return transactions.stream().mapToInt(Transaction::getValue).min().orElse(-1);
    }




}
