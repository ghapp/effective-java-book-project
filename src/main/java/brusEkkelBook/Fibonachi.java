package brusEkkelBook;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fibonachi {
    /**
     * Вводим длину списка фибоначи, поучаем список, for
     * @param count
     * @return
     */
    public static List<Integer> generateFibonachiList(int count){
        List<Integer> tmp = new ArrayList<>(List.of(0, 1));
        for(int i = 2; i < count; i++){
            int next = tmp.get(i-2) + tmp.get(i-1);
            tmp.add(next);
        }
        if(count <= 0 ) return Collections.emptyList();
        if(count == 1) return List.of(0);
        return tmp;
    }

    /**
     * Числа Фибоначи, stream api
     */
    public static List<Integer> generateFibonachiListWithStreamApi(int n){
        return Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1], t[0] + t[1]})
                .limit(n)
                .map(t -> t[0])
                .collect(Collectors.toList());
    }

}
