package smallLogicTasks;

import java.util.List;

public class SearchMinMax {

    /**
     * Method returns minimum value from list
     */
    public static Integer min(List<Integer> list) {
        return list.stream().reduce(Integer.MAX_VALUE, Integer::min);
    }


    /**
     * Method returns maximum value from list
     */
    public static Integer max(List<Integer> list) {
        return list.stream().reduce(Integer.MIN_VALUE, Integer::max);
    }
}
