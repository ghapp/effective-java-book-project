package smallLogicTasks;

import java.util.List;

public class CalculateSumAllElements {


    // Method returns count of elements in list
    public static <E> int calculateCountElements(List<E> elements) {
        return elements.stream().map(d -> 1).reduce(0, Integer::sum);
    }

}
