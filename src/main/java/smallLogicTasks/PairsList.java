package smallLogicTasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PairsList {
    /**
     * Method returns a list of paired elements from two lists, with the use of collectionsApi
     * @example [1, 2, 3] and [4, 5] -> [1, 4], [1, 5], [2, 4], [2, 5], [3, 4], [3, 5]
     *
     */
    public static List<List<Integer>> pairsWithForEach(List<Integer> list1, List<Integer> list2) {
        List<List<Integer>> result = new ArrayList<>();
        list1.forEach(integer -> list2.forEach(value -> {
            List<Integer> list = new ArrayList<>();
            list.add(integer);
            list.add(value);
            result.add(list);
        }));
        return result;
    }

    /**
     * Method returns a list of paired elements from two lists, with the use of streamApi
     * @example [1, 2, 3] and [4, 5] -> [1, 4], [1, 5], [2, 4], [2, 5], [3, 4], [3, 5]
     *
     */
    public static List<List<Integer>> pairsWithStreamApi(List<Integer> list1, List<Integer> list2) {
        return list1.stream().flatMap(l1 -> list2.stream().map(
                l2 -> Arrays.asList(l1,l2)
        )).collect(Collectors.toList());
    }
}
